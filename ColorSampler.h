/**
 * @file ColorSampler.cpp
 * @author Tomas Polasek
 * @brief Simple color sampler with binding to OpenGL.
 */

#ifndef FRACTALEXPLORER_COLORSAMPLER_H
#define FRACTALEXPLORER_COLORSAMPLER_H

#include "Types.h"

/// Simple color sampler with binding to OpenGL.
class ColorSampler
{
public:
    ColorSampler() = default;

    /// Free all acquired resources.
    ~ColorSampler();

    /**
     * Create a sampler from given cairo surface.
     * @param surface Filled Cairo surface.
     */
    void fromCairoSurface(cairo_surface_t *surface);

    /**
     * Use this color sampler as sampler1D.
     * @param id ID of the uniform.
     */
    void use(GLint id);
private:
    /// Free resources.
    void destroy();

    /// ID of the created texture.
    GLuint mTextureId{0};
protected:
};


#endif //FRACTALEXPLORER_COLORSAMPLER_H
