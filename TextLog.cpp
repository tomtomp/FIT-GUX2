//
// Created by tomtomp on 10/7/2017.
//

#include "TextLog.h"

void TextLog::setTextView(GtkTextView *textView)
{
    mTextView = textView;
}

void TextLog::resetTextView()
{
    mTextView = nullptr;
}

const char *TextLog::getMsgPrefix(MsgType type)
{
    switch (type)
    {
        case MsgType::info:
        {
            return "\n[INFO]: ";
        }
        case MsgType::warning:
        {
            return "\n[WARN]: ";
        }
        case MsgType::error:
        {
            return "\n[ERR]: ";
        }
        case MsgType::critical:
        {
            return "\n[CRIT]: ";
        }
    }
}
