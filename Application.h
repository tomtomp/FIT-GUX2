/**
 * @file Application.h
 * @author Tomas Polasek
 * @brief Main application class.
 */

#ifndef FRACTALEXPLORER_APPLICATION_H
#define FRACTALEXPLORER_APPLICATION_H

#include "Types.h"
#include "TextLog.h"
#include "Projection.h"
#include "GLSLProgram.h"
#include "ColorSampler.h"
#include "Plane.h"
#include "ColorChooser.h"

/**
 * @brief Main application class.
 */
class Application
{
public:
    /// Initialization and singleton getter.
    static Application &init();

    Application(const Application &o) = delete;
    Application(Application &&o) = delete;

    Application &operator=(const Application &o) = delete;
    Application &operator=(Application &&o) = delete;

    /**
     * Start the application loop.
     * @return Returns code from the Gtk application.
     */
    int run(int argc, char *argv[]);
private:
    static constexpr float START_MIDDLE_X{0.0};
    static constexpr float START_MIDDLE_Y{0.0};
    static constexpr float START_SCALE{2.0};
    static constexpr float START_JULIA_SEED_RE{0.0};
    static constexpr float START_JULIA_SEED_IM{0.0};
    static constexpr float START_FRACTAL_EXP_RE{2.0};
    static constexpr float START_FRACTAL_EXP_IM{0.0};
    static constexpr unsigned int START_MAX_ITER{100};
    static constexpr int START_COLORING_TYPE{0};
    static constexpr char *COLORING_TYPE_0{"Smooth"};
    static constexpr char *COLORING_TYPE_1{"Gaussian"};
    static constexpr char *COLORING_TYPE_2{"Simple"};

    /// Initialize the application.
    Application();

    /// Create the menu bar.
    static GtkWidget *createMenuBar();

    /// Create the main work area.
    static GtkWidget *createWorkArea();
    static GtkWidget *createWorkPane();
    static GtkWidget *createWorkDrawArea();
    static GtkWidget *createWorkLogArea();
    static GtkWidget *createWorkMenu();
    static GtkWidget *createWorkMenuBasic();
    static GtkWidget *createWorkMenuColor();

    /// Create the bottom status bar.
    static GtkWidget *createStatusBar();

    /// Initialize the mvp matrix to default values.
    static void initializeMvp();

    /// Initialization of UI elements.
    static void activate(GtkApplication *app, gpointer userData);

    /// Callback for glArea intitialization.
    static bool glInit(GtkApplication *app, gpointer userData);
    /// Callback for glArea destruction.
    static bool glDestroy(GtkApplication *app, gpointer userData);
    /// Callback for glArea rendering.
    static bool glRender(GtkApplication *app, gpointer userData);
    /// Callback for glArea resize.
    static bool glResize(GtkApplication *app, GtkAllocation *alloc, void *data);
    /// Callback for glArea button press.
    static bool buttonPress(GtkApplication *app, GdkEvent *event, gpointer userData);
    /// Callback for glArea scroll wheel.
    static bool mouseScroll(GtkApplication *app, GdkEvent *event, gpointer userData);
    /// Julia switch callback.
    static bool juliaSwitch(GtkApplication *app, gpointer userData);
    /// Redraw the glArea.
    static void redraw();
    /// Julia seed scale.
    static bool juliaSeedRe(GtkAdjustment *adj, gpointer userData);
    /// Julia seed scale.
    static bool juliaSeedIm(GtkAdjustment *adj, gpointer userData);
    /// Fractal exponent scale.
    static bool fractalExpRe(GtkAdjustment *adj, gpointer userData);
    /// Fractal exponent scale.
    static bool fractalExpIm(GtkAdjustment *adj, gpointer userData);
    /// Maximum iterations scale..
    static bool maxIterAdj(GtkAdjustment *adj, gpointer userData);
    /// Reload shaders menu callback.
    static void reloadShaders(GtkMenuItem *menuItem, gpointer userData);
    /// Reset view menu callback.
    static void resetView(GtkMenuItem *menuItem, gpointer userData);
    /// Dialog diaplsy callback.
    static void aboutDialog(GtkMenuItem *menuItem, gpointer userData);
    /// Dynamic exponent switch callback.
    static bool dynExpSwitch(GtkApplication *app, gpointer userData);
    /// Colring type change callback.
    static void coloringTypeChange(GtkToggleButton *btn, gpointer userData);

    static struct GlobalData
    {
        // Parameters used in rendering.
        GLSLProgram glProgram;
        ColorSampler glSampler;
        std::unique_ptr<Plane> glPlane;
        Projection glMvp;
        int glMaxIter{START_MAX_ITER};
        bool glDoJulia{false};
        Point glJuliaSeed{START_JULIA_SEED_RE, START_JULIA_SEED_IM};
        Point glFractalExp{START_FRACTAL_EXP_RE, START_FRACTAL_EXP_IM};
        bool glDoDynExp{false};
        int glColoringType{START_COLORING_TYPE};

        // Widgets
        GtkWidget *mainWindow;
        GtkWidget *glArea;
        GtkAdjustment *juliaSeedReAdj{nullptr};
        GtkWidget *juliaSeedReScale{nullptr};
        GtkAdjustment *juliaSeedImAdj{nullptr};
        GtkWidget *juliaSeedImScale{nullptr};
        GtkWidget *fractalExpRe{nullptr};
        GtkWidget *fractalExpIm{nullptr};
        ColorChooser colorChooser;
        TextLog messages;
    } sG;
protected:
};


#endif //FRACTALEXPLORER_APPLICATION_H
