/**
 * @file Application.cpp
 * @author Tomas Polasek
 * @brief Main application class.
 */

#include "Application.h"

Application::GlobalData Application::sG;

Application &Application::init()
{
    static Application app;
    return app;
}

Application::Application()
{
}

int Application::run(int argc, char *argv[])
{
    GtkApplication *app;
    int status;

    app = gtk_application_new("cz.vut.fit.gux.fractalExplorer", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(Application::activate), nullptr);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);

    return status;
}

GtkWidget *Application::createMenuBar()
{
    GtkWidget *menuBar = gtk_menu_bar_new();
    GtkWidget *fileItem = gtk_menu_item_new_with_mnemonic("_File");
    GtkWidget *fileMenu = gtk_menu_new();
    GtkWidget *reloadShadersItem = gtk_menu_item_new_with_label("Reload shaders");
    g_signal_connect(reloadShadersItem, "activate", G_CALLBACK(Application::reloadShaders), nullptr);
    gtk_menu_shell_append(GTK_MENU_SHELL(fileMenu), reloadShadersItem);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(fileItem), fileMenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar), fileItem);

    GtkWidget *viewItem = gtk_menu_item_new_with_mnemonic("_View");
    GtkWidget *viewMenu = gtk_menu_new();
    GtkWidget *resetViewItem = gtk_menu_item_new_with_label("Reset view");
    g_signal_connect(resetViewItem, "activate", G_CALLBACK(Application::resetView), nullptr);
    gtk_menu_shell_append(GTK_MENU_SHELL(viewMenu), resetViewItem);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(viewItem), viewMenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar), viewItem);

    GtkWidget *helpItem = gtk_menu_item_new_with_mnemonic("_Help");
    GtkWidget *helpMenu = gtk_menu_new();
    GtkWidget *aboutHelpItem = gtk_menu_item_new_with_label("About");
    g_signal_connect(aboutHelpItem, "activate", G_CALLBACK(Application::aboutDialog), nullptr);
    gtk_menu_shell_append(GTK_MENU_SHELL(helpMenu), aboutHelpItem);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(helpItem), helpMenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar), helpItem);

    return menuBar;
}

GtkWidget *Application::createWorkArea()
{
    GtkWidget *middleHBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    GtkWidget *middleVSeparator = gtk_separator_new(GTK_ORIENTATION_VERTICAL);

    gtk_box_pack_start(GTK_BOX(middleHBox), createWorkPane(), true, true, 0);
    gtk_box_pack_start(GTK_BOX(middleHBox), middleVSeparator, false, false, 0);
    gtk_box_pack_start(GTK_BOX(middleHBox), createWorkMenu(), false, false, 0);

    return middleHBox;
}

GtkWidget *Application::createWorkPane()
{
    GtkWidget *workBox = gtk_paned_new(GTK_ORIENTATION_VERTICAL);

    gtk_paned_pack1(GTK_PANED(workBox), createWorkDrawArea(), true, true);
    gtk_paned_pack2(GTK_PANED(workBox), createWorkLogArea(), false, false);

    return workBox;
}

GtkWidget *Application::createWorkDrawArea()
{
    GtkWidget *drawArea = gtk_gl_area_new();
    gtk_widget_add_events(drawArea, GDK_BUTTON_PRESS_MASK | GDK_SCROLL_MASK);

    g_signal_connect(drawArea, "realize", G_CALLBACK(Application::glInit), nullptr);
    g_signal_connect(drawArea, "unrealize", G_CALLBACK(Application::glDestroy), nullptr);
    g_signal_connect(drawArea, "render", G_CALLBACK(Application::glRender), nullptr);
    g_signal_connect(drawArea, "size-allocate", G_CALLBACK(Application::glResize), nullptr);
    g_signal_connect(drawArea, "button-press-event", G_CALLBACK(Application::buttonPress), nullptr);
    g_signal_connect(drawArea, "scroll-event", G_CALLBACK(Application::mouseScroll), nullptr);

    sG.glArea = drawArea;

    return drawArea;
}

GtkWidget *Application::createWorkLogArea()
{
    GtkWidget *logBox = gtk_scrolled_window_new(nullptr, nullptr);
    GtkWidget *logArea = gtk_text_view_new();
    gtk_text_view_set_editable(GTK_TEXT_VIEW(logArea), false);
    sG.messages.setTextView(GTK_TEXT_VIEW(logArea));

    gtk_container_add(GTK_CONTAINER(logBox), logArea);
    gtk_widget_set_size_request(logBox, 0, 100);

    return logBox;
}

GtkWidget *Application::createWorkMenu()
{
    GtkWidget *menuTabs = gtk_notebook_new();
    //gtk_widget_set_size_request(menuTabs, 100, 0);

    GtkWidget *basicLabel = gtk_label_new("Fractal");
    gtk_notebook_append_page(GTK_NOTEBOOK(menuTabs), createWorkMenuBasic(), basicLabel);

    GtkWidget *colorLabel = gtk_label_new("Color");
    gtk_notebook_append_page(GTK_NOTEBOOK(menuTabs), createWorkMenuColor(), colorLabel);

    return menuTabs;
}

GtkWidget *Application::createWorkMenuBasic()
{
    GtkWidget *basicSettings = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    GtkWidget *juliaFrame = gtk_frame_new("Julia set");
    //gtk_frame_set_label_align(GTK_FRAME(juliaFrame), 0.1, 0);
    GtkWidget *juliaGrid = gtk_grid_new();
    gtk_grid_set_column_homogeneous(GTK_GRID(juliaGrid), true);
    gtk_grid_set_column_spacing(GTK_GRID(juliaGrid), 12);
    gtk_grid_set_row_spacing(GTK_GRID(juliaGrid), 6);

    GtkWidget *juliaSwitchLabel = gtk_label_new("Enable");
    gtk_widget_set_halign(juliaSwitchLabel, GTK_ALIGN_END);
    gtk_grid_attach(
            GTK_GRID(juliaGrid), juliaSwitchLabel,
            0, 0,
            1, 1
    );
    GtkWidget *juliaSwitch = gtk_switch_new();
    g_signal_connect(juliaSwitch, "state-set", G_CALLBACK(Application::juliaSwitch), nullptr);
    gtk_grid_attach(
            GTK_GRID(juliaGrid), juliaSwitch,
            1, 0,
            1, 1
    );

    GtkWidget *juliaSeedReLabel = gtk_label_new("Seed [Re]");
    gtk_widget_set_halign(juliaSeedReLabel, GTK_ALIGN_END);
    gtk_grid_attach(
            GTK_GRID(juliaGrid), juliaSeedReLabel,
            0, 1,
            1, 1
    );
    GtkAdjustment *juliaSeedAdjRe = gtk_adjustment_new(START_JULIA_SEED_RE, -2.0, 2.0, 0.1, 1.0, 0.0);
    GtkWidget *juliaSeedRe = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, juliaSeedAdjRe);
    sG.juliaSeedReAdj = juliaSeedAdjRe;
    sG.juliaSeedReScale = juliaSeedRe;
    g_signal_connect(juliaSeedAdjRe, "value-changed", G_CALLBACK(Application::juliaSeedRe), nullptr);
    gtk_widget_set_sensitive(juliaSeedRe, false);
    gtk_grid_attach(
            GTK_GRID(juliaGrid), juliaSeedRe,
            1, 1,
            1, 1
    );

    GtkWidget *juliaSeedImLabel = gtk_label_new("Seed [Im]");
    gtk_widget_set_halign(juliaSeedImLabel, GTK_ALIGN_END);
    gtk_grid_attach(
            GTK_GRID(juliaGrid), juliaSeedImLabel,
            0, 2,
            1, 1
    );
    GtkAdjustment *juliaSeedAdjIm = gtk_adjustment_new(START_JULIA_SEED_IM, -2.0, 2.0, 0.1, 1.0, 0.0);
    GtkWidget *juliaSeedIm = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, juliaSeedAdjIm);
    sG.juliaSeedImAdj = juliaSeedAdjIm;
    sG.juliaSeedImScale = juliaSeedIm;
    g_signal_connect(juliaSeedAdjIm, "value-changed", G_CALLBACK(Application::juliaSeedIm), nullptr);
    gtk_widget_set_sensitive(juliaSeedIm, false);
    gtk_grid_attach(
            GTK_GRID(juliaGrid), juliaSeedIm,
            1, 2,
            1, 1
    );

    gtk_container_add(GTK_CONTAINER(juliaFrame), juliaGrid);

    GtkWidget *fractalFrame = gtk_frame_new("Fractal");
    GtkWidget *fractalGrid = gtk_grid_new();
    gtk_grid_set_column_homogeneous(GTK_GRID(fractalGrid), true);
    gtk_grid_set_column_spacing(GTK_GRID(fractalGrid), 12);
    gtk_grid_set_row_spacing(GTK_GRID(fractalGrid), 6);

    GtkWidget *dynamicExpLabel = gtk_label_new("Dynamic exp");
    gtk_widget_set_halign(dynamicExpLabel, GTK_ALIGN_END);
    gtk_grid_attach(
        GTK_GRID(fractalGrid), dynamicExpLabel,
        0, 0,
        1, 1
    );
    GtkWidget *fractalExpSwitch = gtk_switch_new();
    gtk_switch_set_state(GTK_SWITCH(fractalExpSwitch), false);
    g_signal_connect(fractalExpSwitch, "state-set", G_CALLBACK(Application::dynExpSwitch), nullptr);
    gtk_grid_attach(
        GTK_GRID(fractalGrid), fractalExpSwitch,
        1, 0,
        1, 1
    );

    GtkWidget *fractalExpReLabel = gtk_label_new("Exp [Re]");
    gtk_widget_set_halign(fractalExpReLabel, GTK_ALIGN_END);
    gtk_grid_attach(
            GTK_GRID(fractalGrid), fractalExpReLabel,
            0, 1,
            1, 1
    );
    GtkAdjustment *fractalExpAdjRe = gtk_adjustment_new(START_FRACTAL_EXP_RE, -100.0, 100.0, 0.1, 1.0, 0.0);
    sG.fractalExpRe = gtk_spin_button_new(fractalExpAdjRe, 0.1, 2);
    g_signal_connect(fractalExpAdjRe, "value-changed", G_CALLBACK(Application::fractalExpRe), nullptr);
    gtk_widget_set_sensitive(sG.fractalExpRe, false);
    gtk_grid_attach(
            GTK_GRID(fractalGrid), sG.fractalExpRe,
            1, 1,
            1, 1
    );

    GtkWidget *fractalExpImLabel = gtk_label_new("Exp [Im]");
    gtk_widget_set_halign(fractalExpImLabel, GTK_ALIGN_END);
    gtk_grid_attach(
            GTK_GRID(fractalGrid), fractalExpImLabel,
            0, 2,
            1, 1
    );
    GtkAdjustment *fractalExpAdjIm = gtk_adjustment_new(START_FRACTAL_EXP_IM, -100.0, 100.0, 0.1, 1.0, 0.0);
    sG.fractalExpIm = gtk_spin_button_new(fractalExpAdjIm, 0.1, 2);
    g_signal_connect(fractalExpAdjIm, "value-changed", G_CALLBACK(Application::fractalExpIm), nullptr);
    gtk_widget_set_sensitive(sG.fractalExpIm, false);
    gtk_grid_attach(
            GTK_GRID(fractalGrid), sG.fractalExpIm,
            1, 2,
            1, 1
    );

    GtkWidget *maxIterLabel = gtk_label_new("Iter limit");
    gtk_widget_set_halign(maxIterLabel, GTK_ALIGN_END);
    gtk_grid_attach(
            GTK_GRID(fractalGrid), maxIterLabel,
            0, 3,
            1, 1
    );
    // TODO - change to integer only.
    GtkAdjustment *maxIterAdj = gtk_adjustment_new(START_MAX_ITER, 1, 255, 1, 1.0, 0.0);
    GtkWidget *maxIter = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, maxIterAdj);
    g_signal_connect(maxIterAdj, "value-changed", G_CALLBACK(Application::maxIterAdj), nullptr);
    gtk_grid_attach(
            GTK_GRID(fractalGrid), maxIter,
            1, 3,
            1, 1
    );

    gtk_container_add(GTK_CONTAINER(fractalFrame), fractalGrid);

    gtk_box_pack_start(GTK_BOX(basicSettings), fractalFrame, false, false, 6);
    gtk_box_pack_start(GTK_BOX(basicSettings), juliaFrame, false, false, 6);

    return basicSettings;
}

GtkWidget *Application::createWorkMenuColor()
{
    GtkWidget *colorSettings = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    sG.colorChooser.init(&sG.glSampler, sG.glArea);
    gtk_box_pack_start(GTK_BOX(colorSettings), sG.colorChooser.parent(), false, false, 6);

    GtkWidget *colorTypeFrame = gtk_frame_new("Coloring method");
    GtkWidget *colorTypeGrid = gtk_grid_new();
    gtk_grid_set_column_homogeneous(GTK_GRID(colorTypeGrid), true);
    gtk_grid_set_column_spacing(GTK_GRID(colorTypeGrid), 12);
    gtk_grid_set_row_spacing(GTK_GRID(colorTypeGrid), 6);

    GtkWidget *colorButton0 = gtk_radio_button_new_with_label(nullptr, COLORING_TYPE_0);
    GtkWidget *colorButton1 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(colorButton0),
                                                                          COLORING_TYPE_1);
    GtkWidget *colorButton2 = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(colorButton0),
                                                                          COLORING_TYPE_2);

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(colorButton0), true);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(colorButton1), false);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(colorButton2), false);

    g_signal_connect(colorButton0, "toggled", G_CALLBACK(Application::coloringTypeChange),
                     reinterpret_cast<gpointer>(0));
    g_signal_connect(colorButton1, "toggled", G_CALLBACK(Application::coloringTypeChange),
                     reinterpret_cast<gpointer>(1));
    g_signal_connect(colorButton2, "toggled", G_CALLBACK(Application::coloringTypeChange),
                     reinterpret_cast<gpointer>(2));

    gtk_grid_attach(GTK_GRID(colorTypeGrid), colorButton0, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(colorTypeGrid), colorButton1, 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(colorTypeGrid), colorButton2, 0, 2, 1, 1);
    gtk_container_add(GTK_CONTAINER(colorTypeFrame), colorTypeGrid);

    gtk_box_pack_start(GTK_BOX(colorSettings), colorTypeFrame, false, false, 6);

    return colorSettings;
}

GtkWidget *Application::createStatusBar()
{
    GtkWidget *statusBar = gtk_statusbar_new();
    return statusBar;
}

void Application::activate(GtkApplication* app, gpointer user_data)
{
    sG.mainWindow = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(sG.mainWindow), "FractalExplorer");
    gtk_window_set_default_size(GTK_WINDOW(sG.mainWindow), 800, 600);

    GtkWidget *mainVBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    gtk_container_add(GTK_CONTAINER(sG.mainWindow), mainVBox);

    gtk_box_pack_start(GTK_BOX(mainVBox), createMenuBar(), FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(mainVBox), createWorkArea(), TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(mainVBox), createStatusBar(), FALSE, FALSE, 0);

    gtk_widget_show_all(sG.mainWindow);

    initializeMvp();

    sG.messages.logMessage(MsgType::info, "Initialized window.");
}

void Application::initializeMvp()
{
    int width = gtk_widget_get_allocated_width(sG.glArea);
    int height = gtk_widget_get_allocated_height(sG.glArea);
    sG.glMvp.setScreenSize(width, height);
    sG.glMvp.setMiddle({START_MIDDLE_X, START_MIDDLE_Y});
    sG.glMvp.setScale(START_SCALE);
}

bool Application::glInit(GtkApplication *app, gpointer userData)
{
    sG.messages.logMessage(MsgType::info, "Initializing OpenGL context...");

    gtk_gl_area_make_current(GTK_GL_AREA(sG.glArea));
    if (gtk_gl_area_get_error(GTK_GL_AREA(sG.glArea)) != nullptr)
    {
        throw std::runtime_error("Unable to initialize the glArea!");
    }

    glEnable(GL_MULTISAMPLE_ARB);

    try {
        sG.glProgram = GLSLProgram{
                {GL_VERTEX_SHADER,   std::string("basic.vs")},
                {GL_FRAGMENT_SHADER, std::string("basic.fs")}
        };
    } catch (std::runtime_error &e) {
        sG.messages.logMessage(MsgType::error, "Unable to compile GLSL program: ");
        sG.messages.logMessage(MsgType::error, e.what());
    }
    sG.messages.logMessage(MsgType::info, "Compiled GLSL program.");

    sG.glProgram.use();

    try {
        //sG.glPlane = std::make_unique<Plane>();
        sG.glPlane = std::unique_ptr<Plane>(new Plane);
    } catch (std::runtime_error &e) {
        sG.messages.logMessage(MsgType::error, "Unable to create Plane renderable: ");
        sG.messages.logMessage(MsgType::error, e.what());
    }

    sG.colorChooser.updateSampler();

    return true;
}

bool Application::glDestroy(GtkApplication *app, gpointer userData)
{
    std::cout << "glArea destruction" << std::endl;
    return true;
}

bool Application::glRender(GtkApplication *app, gpointer userData)
{
    glClearColor(0.5, 0.5, 0.5, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    sG.glProgram.use();

    // TODO - Cache uniform ID?
    sG.glMvp.use(sG.glProgram.getUniformLocation("mvp"));
    glUniform1i(sG.glProgram.getUniformLocation("maxIter"), sG.glMaxIter);
    glUniform1i(sG.glProgram.getUniformLocation("drawJulia"), sG.glDoJulia);
    glUniform2fv(sG.glProgram.getUniformLocation("juliaSeed"), 1, sG.glJuliaSeed.data());
    glUniform2fv(sG.glProgram.getUniformLocation("fractalExp"), 1, sG.glFractalExp.data());
    sG.glSampler.use(sG.glProgram.getUniformLocation("colorS"));
    glUniform1i(sG.glProgram.getUniformLocation("dynExp"), sG.glDoDynExp);
    glUniform1i(sG.glProgram.getUniformLocation("coloringType"), sG.glColoringType);

    sG.glPlane->render();

    glFlush();

    return true;
}

bool Application::glResize(GtkApplication *app, GtkAllocation *alloc, void *data)
{
    sG.glMvp.setScreenSize(alloc->width, alloc->height);

    return true;
}

bool Application::buttonPress(GtkApplication *app, GdkEvent *event, gpointer userData)
{
    GdkEventButton *info = reinterpret_cast<GdkEventButton*>(event);

    if (info->state & GDK_CONTROL_MASK)
    { // Control modifier
        Point sp = sG.glMvp.screenFromMouse(
                {static_cast<float>(info->x),
                 static_cast<float>(info->y)}
        );
        gtk_adjustment_set_value(sG.juliaSeedReAdj, sp.x());
        gtk_adjustment_set_value(sG.juliaSeedImAdj, sp.y());
    }
    else
    {
        sG.glMvp.setMiddleFromMouse(
                {static_cast<float>(info->x),
                 static_cast<float>(info->y)}
        );
    }
    redraw();

    return true;
}

bool Application::mouseScroll(GtkApplication *app, GdkEvent *event, gpointer userData)
{
    GdkEventScroll *info = reinterpret_cast<GdkEventScroll*>(event);
    if (info->direction == GDK_SCROLL_UP)
    {
        sG.glMvp.zoom(0.9);
    }
    else if (info->direction == GDK_SCROLL_DOWN)
    {
        sG.glMvp.zoom(1.1);
    }
    redraw();

    return true;
}

bool Application::juliaSwitch(GtkApplication *app, gpointer userData)
{
    sG.glDoJulia = !sG.glDoJulia;
    gtk_widget_set_sensitive(sG.juliaSeedReScale, sG.glDoJulia);
    gtk_widget_set_sensitive(sG.juliaSeedImScale, sG.glDoJulia);
    //gtk_widget_set_visible(sG.juliaSeedReScale, sG.glDoJulia);
    //gtk_widget_set_visible(sG.juliaSeedImScale, sG.glDoJulia);
    redraw();
    return true;
}

void Application::redraw()
{
    gtk_widget_queue_draw(sG.glArea);
}

bool Application::juliaSeedRe(GtkAdjustment *adj, gpointer userData)
{
    sG.glJuliaSeed.x() = gtk_adjustment_get_value(adj);
    redraw();
    return true;
}

bool Application::juliaSeedIm(GtkAdjustment *adj, gpointer userData)
{
    sG.glJuliaSeed.y() = gtk_adjustment_get_value(adj);
    redraw();
    return true;
}

bool Application::fractalExpRe(GtkAdjustment *adj, gpointer userData)
{
    sG.glFractalExp.x() = gtk_adjustment_get_value(adj);
    redraw();
    return true;
}

bool Application::fractalExpIm(GtkAdjustment *adj, gpointer userData)
{
    sG.glFractalExp.y() = gtk_adjustment_get_value(adj);
    redraw();
    return true;
}

bool Application::maxIterAdj(GtkAdjustment *adj, gpointer userData)
{
    sG.glMaxIter = gtk_adjustment_get_value(adj);
    redraw();
    return true;
}

void Application::reloadShaders(GtkMenuItem *menuItem, gpointer userData)
{
    try {
        GLSLProgram newProgram{
                {GL_VERTEX_SHADER,   std::string("basic.vs")},
                {GL_FRAGMENT_SHADER, std::string("basic.fs")}
        };
        sG.glProgram.swap(newProgram);
        sG.messages.logMessage(MsgType::info, "Reloaded shaders.");
        redraw();
    } catch(std::runtime_error &e) {
        sG.messages.logMessage(MsgType::error, "Unable to compile GLSL program: ");
        sG.messages.logMessage(MsgType::error, e.what());
    }
}

void Application::resetView(GtkMenuItem *menuItem, gpointer userData)
{
    initializeMvp();
    sG.messages.logMessage(MsgType::info, "Reset view.");
    redraw();
}

void Application::aboutDialog(GtkMenuItem *menuItem, gpointer userData)
{
    const char *authors[] = {
            "Tomas Polasek <xpolas34@fit.vutbr.cz>",
            nullptr,
    };
    gtk_show_about_dialog(
        GTK_WINDOW(sG.mainWindow),
        "program-name", "FractalExplorer",
        "version", "0.1",
        "authors", authors,
        "comments", "Tool used for exploring fractals.",
        //"logo-icon-name", GTK_STOCK_FULLSCREEN,
        "logo-icon-name", g_dgettext("gtk30", "view-fullscreen"),
        nullptr
    );
}

bool Application::dynExpSwitch(GtkApplication *app, gpointer userData)
{
    sG.glDoDynExp = !sG.glDoDynExp;
    gtk_widget_set_sensitive(sG.fractalExpRe, sG.glDoDynExp);
    gtk_widget_set_sensitive(sG.fractalExpIm, sG.glDoDynExp);
    redraw();
    return true;
}

void Application::coloringTypeChange(GtkToggleButton *btn, gpointer userData)
{
    if (gtk_toggle_button_get_active(btn))
    {
        sG.glColoringType = reinterpret_cast<intptr_t>(userData);
        redraw();
    }
}
