/**
 * @file Projection.cpp
 * @author Tomas Polasek
 * @brief 
 */

#include "Projection.h"

Projection::Projection() :
    mDirty{true}, mMiddle{0.0, 0.0},
    mAspect{0.0}, mScale{0.0},
    mWidth{0}, mHeight{0}
{ }

GLfloat *Projection::data()
{
    if (mDirty)
    {
        recalculate();
    }
    return mMat;
}

const GLfloat *Projection::data() const
{
    if (mDirty)
    {
        throw std::runtime_error("Unable to recalculate constant Projection!");
    }
    return mMat;
}

void Projection::setMiddleFromMouse(const Point &m)
{
    mMiddle = screenFromMouse(m);

    mDirty = true;
}

void Projection::setScreenSize(int width, int height)
{
    mWidth = width;
    mHeight = height;
    mAspect = static_cast<float>(width) / height;
    mDirty = true;
}

Point Projection::screenFromMouse(const Point &m)
{
    // Recalculate to <-1.0, 1.0>
    float xIn{(m.x() / mWidth) * 2.0f - 1.0f};
    float yIn{(m.y() / mHeight) * -2.0f + 1.0f};

    // Calculate screen position.
    return Point{
            xIn * mAspect * mScale + mMiddle.x(),
            yIn * mScale + mMiddle.y()
    };
}

void Projection::use(GLint uniformLoc)
{
    glUniformMatrix4fv(uniformLoc, 1, GL_FALSE, data());
}

void Projection::recalculate()
{
    mMat[0] = mAspect * mScale;     mMat[1] = 0.0;              mMat[2] = 0.0;  mMat[3] = 0.0;
    mMat[4] = 0.0;                  mMat[5] = mScale;           mMat[6] = 0.0;  mMat[7] = 0.0;
    mMat[8] = 0.0;                  mMat[9] = 0.0;              mMat[10] = 1.0; mMat[11] = 0.0;
    mMat[12] = mMiddle.x();         mMat[13] = mMiddle.y();     mMat[14] = 0.0; mMat[15] = 1.0;

    mDirty = false;
}
