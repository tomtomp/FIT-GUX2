# Source: https://stackoverflow.com/questions/2481269/how-to-make-a-simple-c-makefile
CXX=g++
RM=rm -f
#CXXFLAGS=-O2 -Wall -DGTK_DISABLE_DEPRECATED=1 -DGDK_DISABLE_DEPRECATED=1 -DG_DISABLE_DEPRECATED=1 `pkg-config --cflags gtk+-3.0 epoxy`
CXXFLAGS=-std=c++11 -O2 -Wall `pkg-config --cflags gtk+-3.0 epoxy`
LDLIBS=`pkg-config --libs gtk+-3.0 epoxy`
#LDFLAGS=-std=c++11 -nostartfiles
LDFLAGS=-std=c++11

SRCS=main.cpp Projection.cpp Plane.cpp FramebufferImage.cpp TextLog.cpp ColorSampler.cpp ColorChooser.cpp Application.cpp
#OBJS=$(subst .cpp,.o,$(SRCS))
OBJS=main.o Projection.o Plane.o FramebufferImage.o TextLog.o ColorSampler.o ColorChooser.o Application.o
all: FractalExplorer

FractalExplorer: $(OBJS)
	$(CXX) $(LDFLAGS) -o FractalExplorer $(OBJS) $(LDLIBS)

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS) FractalExplorer

include: .depend

