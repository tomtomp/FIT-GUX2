/**
 * @file ColorChooser.cpp
 * @author Tomas Polasek
 * @brief Color chooser widget.
 */

#include "ColorChooser.h"

const GdkRGBA ColorChooser::START_COLOR{1.0, 1.0, 1.0, 1.0};
const GdkRGBA ColorChooser::START_FIRST_COLOR{1.0, 1.0, 1.0, 1.0};
const GdkRGBA ColorChooser::START_SECOND_COLOR{0.0, 0.0, 0.0, 1.0};

void ColorChooser::init(ColorSampler *cs, GtkWidget *redraw)
{
    mCS = cs;
    mRedraw = redraw;
    createUI();
}

GtkWidget *ColorChooser::parent()
{
    return mParent;
}

void ColorChooser::createUI()
{
    mParent = gtk_frame_new("Color chooser");
    GtkWidget *ccBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 12);

    GtkWidget *colorSampler = gtk_drawing_area_new();
    g_signal_connect(colorSampler, "draw", G_CALLBACK(ColorChooser::samplerDraw), this);
    gtk_widget_set_size_request(colorSampler, 0, 30);
    gtk_box_pack_start(GTK_BOX(ccBox), colorSampler, true, true, 6);

    GtkWidget *scrollWindow = gtk_scrolled_window_new(nullptr, nullptr);
    gtk_widget_set_size_request(scrollWindow, 0, 100);
    mColorList = gtk_list_box_new();
    gtk_list_box_set_selection_mode(GTK_LIST_BOX(mColorList), GTK_SELECTION_NONE);
    gtk_container_add(GTK_CONTAINER(scrollWindow), mColorList);
    gtk_box_pack_start(GTK_BOX(ccBox), scrollWindow, false, false, 6);

    addColor(START_FIRST_COLOR);
    addColor(START_SECOND_COLOR);

    GtkWidget *buttonBox = gtk_grid_new();
    gtk_grid_set_column_homogeneous(GTK_GRID(buttonBox), true);
    gtk_grid_set_column_spacing(GTK_GRID(buttonBox), 12);
    gtk_grid_set_row_spacing(GTK_GRID(buttonBox), 6);

    mAddButton = gtk_button_new_with_label("Add");
    g_signal_connect(mAddButton, "clicked", G_CALLBACK(ColorChooser::addColorBtn), this);
    mRemoveButton = gtk_button_new_with_label("Remove");
    g_signal_connect(mRemoveButton, "clicked", G_CALLBACK(ColorChooser::removeColorBtn), this);
    gtk_widget_set_sensitive(mRemoveButton, false);

    gtk_grid_attach(GTK_GRID(buttonBox), mAddButton, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(buttonBox), mRemoveButton, 2, 0, 1, 1);

    gtk_box_pack_end(GTK_BOX(ccBox), buttonBox, false, false, 6);

    gtk_container_add(GTK_CONTAINER(mParent), ccBox);
}

void ColorChooser::addColor(GdkRGBA color)
{
    GtkWidget *rowBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 12);
    //std::string name{std::string("Color #") + std::to_string(mColorButtons.size() + 1)};
    std::ostringstream ss;
    ss << "Color #" << mColorButtons.size() + 1;
    gtk_box_pack_start(GTK_BOX(rowBox), gtk_label_new(ss.str().c_str()), false, false, 6);
    GtkWidget *btn = gtk_color_button_new_with_rgba(&color);
    gtk_box_pack_end(GTK_BOX(rowBox), btn, false, false, 6);
    g_signal_connect(btn, "color-set", G_CALLBACK(ColorChooser::colorChanged), this);
    gtk_container_add(GTK_CONTAINER(mColorList), rowBox);
    gtk_widget_show_all(mColorList);

    mColorButtons.push_back(btn);

    if (mColorButtons.size() > MIN_COLORS)
    {
        gtk_widget_set_sensitive(mRemoveButton, true);
    }
}

void ColorChooser::removeColor()
{
    if (mColorButtons.size() > 2)
    {
        gtk_container_remove(GTK_CONTAINER(mColorList),
                             GTK_WIDGET(gtk_list_box_get_row_at_index(GTK_LIST_BOX(mColorList), mColorButtons.size() - 1)));
        mColorButtons.pop_back();

        if (mColorButtons.size() <= MIN_COLORS)
        {
            gtk_widget_set_sensitive(mRemoveButton, false);
        }
    }
}

cairo_pattern_t *ColorChooser::createCairoPattern(float width)
{
    float step = 1.0f / mColorButtons.size();
    cairo_pattern_t *pattern = cairo_pattern_create_linear(0.0, 0.0, width, 0.0);
    for (unsigned int iii = 0; iii < mColorButtons.size(); ++iii)
    {
        GdkRGBA c;
        gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(mColorButtons[iii]), &c);

        cairo_pattern_add_color_stop_rgba(
            pattern,  step * iii,
            c.red,
            c.green,
            c.blue,
            c.alpha
        );
    }

    return pattern;
}

void ColorChooser::updateSampler()
{
    cairo_pattern_t *pattern = createCairoPattern(PATTERN_WIDTH);

    cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, PATTERN_WIDTH, 1);
    cairo_t *crp = cairo_create(surface);
    cairo_rectangle(crp, 0.0, 0.0, PATTERN_WIDTH, 1.0);
    cairo_set_source(crp, pattern);
    cairo_fill(crp);
    cairo_surface_flush(surface);

    mCS->fromCairoSurface(surface);

    cairo_surface_destroy(surface);
    cairo_pattern_destroy(pattern);

    gtk_widget_queue_draw(mRedraw);
}

bool ColorChooser::samplerDraw(GtkWidget *app, cairo_t *cr, gpointer userData)
{
    auto *cc = reinterpret_cast<ColorChooser*>(userData);

    GtkWidget *drawArea = GTK_WIDGET(app);
    auto width = gtk_widget_get_allocated_width(drawArea);
    auto height = gtk_widget_get_allocated_height(drawArea);

    cairo_pattern_t *pattern = cc->createCairoPattern(width);

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_set_source(cr, pattern);
    cairo_fill(cr);

    cairo_pattern_destroy(pattern);

    return false;
}

void ColorChooser::colorChanged(GtkColorButton *btn, gpointer userData)
{
    auto *cc = reinterpret_cast<ColorChooser*>(userData);
    cc->updateSampler();
}

void ColorChooser::addColorBtn(GtkButton *btn, gpointer userData)
{
    auto *cc = reinterpret_cast<ColorChooser*>(userData);
    cc->addColor(START_COLOR);
    cc->updateSampler();
    gtk_widget_queue_draw(cc->mParent);
}

void ColorChooser::removeColorBtn(GtkButton *btn, gpointer userData)
{
    auto *cc = reinterpret_cast<ColorChooser*>(userData);
    cc->removeColor();
    cc->updateSampler();
    gtk_widget_queue_draw(cc->mParent);
}
