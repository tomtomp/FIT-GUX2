//
// Created by tomtomp on 10/7/2017.
//

#ifndef FRACTALEXPLORER_TEXTLOG_H
#define FRACTALEXPLORER_TEXTLOG_H

#include "Types.h"

enum class MsgType
{
    info,
    warning,
    error,
    critical
};

/// Wrapper around GtkTextBuffer used for logging messages.
class TextLog
{
public:
    /// Initialize the TextLog.
    TextLog() = default;

    TextLog(const TextLog &o) = delete;
    TextLog(TextLog &&o) = delete;

    TextLog &operator=(const TextLog &r) = delete;
    TextLog &operator=(TextLog &&r) = delete;

    /// Use given text view owned by outside widget.
    void setTextView(GtkTextView *textView);

    /// Reset the text view used by this log to nullptr.
    void resetTextView();

    /**
     * Log given message and update inner text buffer, if present.
     * @tparam Appendable Type which can be appended to a string.
     * @param type Type of message.
     * @param msg Message itself.
     */
    template <typename Appendable>
    void logMessage(MsgType type, const Appendable &msg);
private:
    /// Update inner buffer with current messages.
    void updateBuffer();

    /// Get message prefix string for given message type.
    const char *getMsgPrefix(MsgType type);

    /// Text view representing the log.
    GtkTextView *mTextView{nullptr};
protected:
};

// Template implementation.

template<typename Appendable>
void TextLog::logMessage(MsgType type, const Appendable &msg)
{
    std::string message;
    message.append(getMsgPrefix(type));
    message.append(msg);

    // Source: https://mail.gnome.org/archives/gtk-list/2007-May/msg00034.html
    GtkTextBuffer *tb = gtk_text_view_get_buffer(mTextView);

    GtkTextMark *mark = gtk_text_buffer_get_insert(tb);
    GtkTextIter iter;

    gtk_text_buffer_get_end_iter(tb, &iter);
    gtk_text_buffer_move_mark(tb, mark, &iter);
    gtk_text_buffer_insert_at_cursor(tb, message.c_str(), message.length());
    gtk_text_view_scroll_to_mark(mTextView, mark, 0.0, true, 0.5, 1.0);
}

#endif //FRACTALEXPLORER_TEXTLOG_H
