#version 330 core

uniform int maxIter;
uniform bool drawJulia;
uniform vec2 juliaSeed;
uniform bool dynExp;
uniform vec2 fractalExp;
uniform int coloringType;
#define COLOR_SMOOTH 0
#define COLOR_GAUSS 1
#define COLOR_NORMAL 2

uniform sampler1D colorS;

in vec2 fragPos;

layout(location = 0) out vec4 color;

/**
 * Uses https://en.wikipedia.org/wiki/Complex_logarithm#Definition_of_principal_value
 * log(z) = log(sqrt(z.x^2 + z.y^2)) + i*atan(z.y, z.x) =>
 *  => 1/2 * log(z.x^2 + z.y^2) + i*atan(z.y, z.x)
 */
vec2 clog(vec2 z)
{
    return vec2(
        0.5 * log(z.x * z.x + z.y * z.y),
        atan(z.y, z.x)
    );
}

/**
 * a * b = a.x * b.x - a.y * b.y + i (a.x * b.y + a.y * b.x)
 */
vec2 cmul(vec2 a, vec2 b)
{
    return vec2(
        a.x * b.x - a.y * b.y,
        a.x * b.y + a.y * b.x
    );
}

/**
 * e ^ (x + i*y) = e^x * e^(i*y) = exp(x) * cos(y) + i * exp(x) * sin(y)
 */
vec2 cexp(vec2 z)
{
    return vec2(
        exp(z.x) * cos(z.y),
        exp(z.x) * sin(z.y)
    );
}

/**
 * Uses https://en.wikipedia.org/wiki/Exponentiation#Powers_of_complex_numbers
 * e ^ (e * log(z))
 */
vec2 cpow(vec2 z, vec2 e)
{
    return cexp(cmul(e, clog(z)));
}

float giTrap(vec2 z)
{
    return length(abs(z - round(z)));
}

float gxlTrap(vec2 z)
{
    return abs(z.x - round(z.x));
}

int staticExp2(inout vec2 z, in vec2 c, out float dist)
{
    int iter;
    dist = giTrap(z);
    for (iter = 0; iter < maxIter; ++iter)
    {
        // z^2 + c
        z = vec2(z.x * z.x - z.y * z.y, 2.0 * z.x * z.y) + c;
        dist = max(giTrap(z), dist);
        if (length(z) > 4.0)
            break;
    }

    return iter;
}

int dynamicExp(inout vec2 z, in vec2 c, in vec2 exp, out float dist)
{
    int iter;
    dist = giTrap(z);
    for (iter = 0; iter < maxIter; ++iter)
    {
        // z^{fractalExp} + c
        z = cpow(z, exp) + c;
        dist = max(giTrap(z), dist);
        if (length(z) > 4.0)
            break;
    }

    return iter;
}

void main()
{
    vec2 c = vec2(fragPos.xy);
    vec2 z = c;
    if (drawJulia)
    {
        c = juliaSeed;
    }

    float dist;
    int iter;
    float control;
    vec2 expUsed;

    if (dynExp)
    {
        expUsed = fractalExp;
        iter = dynamicExp(z, c, expUsed, dist);
    }
    else
    {
        expUsed = vec2(2.0, 0.0);
        iter = staticExp2(z, c, dist);
    }

    if (iter >= maxIter)
    {
        control = 1.0;
    }
    else
    {
        switch (coloringType)
        {
            case COLOR_SMOOTH:
            {
                // https://linas.org/art-gallery/escape/smooth.html
                control = (iter + 1.0 - log(log(length(z)) / log(length(expUsed)))) / maxIter;
                break;
            }

            case COLOR_GAUSS:
            {
                control = 1.0 - dist;
                break;
            }
            case COLOR_NORMAL:
            default:
            {
                control = float(iter) / maxIter;
                break;
            }
        }
    }

    color = vec4(texture(colorS, control).xyz, 1.0);
}
