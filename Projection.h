/**
 * @file Projection.h
 * @author Tomas Polasek
 * @brief 
 */

#ifndef FRACTALEXPLORER_PROJECTION_H
#define FRACTALEXPLORER_PROJECTION_H

#include "Types.h"

/// Simple orthogonal projection matrix
class Projection
{
public:
    Projection();

    Projection(const Projection &o) = default;
    Projection(Projection &&o) = default;

    Projection &operator=(const Projection &o) = default;
    Projection &operator=(Projection &&o) = default;

    /**
     * Inner matrix pointer getter.
     * @warning May trigger recalculation!
     * @return Returns pointer to he matrix.
     */
    GLfloat *data();

    /**
     * Inner matrix pointer getter.
     * @throws std::runtime_error If recalculation is required,
     *   throws an exception.
     * @return Returns pointer to he matrix.
     */
    const GLfloat *data() const;

    /// Set the middle point.
    void setMiddle(const Point &m)
    { mMiddle = m; mDirty = true; }

    /// Set the middle point from mouse position.
    void setMiddleFromMouse(const Point &m);

    /// Set the scaling factor.
    void setScale(float scale)
    { mScale = scale; mDirty = true; }

    /// Set the size of the screen and calculate aspect ratio.
    void setScreenSize(int width, int height);

    /// Get screen position from mouse position.
    Point screenFromMouse(const Point &m);

    /// Perform the zoom function.
    void zoom(float change)
    { mScale *= change; mDirty = true; }

    /// Is the matrix dirty?
    bool isDirty() const
    { return mDirty; }

    /**
     * Use this matrix.
     * @param uniformLoc Location of the matrix uniform.
     */
    void use(GLint uniformLoc);

    /// Recalculate the inner matrix.
    void recalculate();
private:
    /// Number of elements in inner matrix.
    static constexpr unsigned int NUM_ELEMENTS{16};

    /// Inner matrix.
    GLfloat mMat[NUM_ELEMENTS];
    /// Dirty flag for matrix recomputation.
    bool mDirty;

    /// Where is the middle.
    Point mMiddle;
    /// Aspect ratio.
    float mAspect;
    /// Scaling factor.
    float mScale;
    /// Width of the screen.
    int mWidth;
    /// Height of the screen.
    int mHeight;
protected:
};


#endif //FRACTALEXPLORER_PROJECTION_H
