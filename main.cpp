#include <iostream>
#include "Application.h"

int main(int argc, char **argv)
{
    try {
        Application &app{Application::init()};
        return app.run(argc, argv);
    } catch (std::runtime_error &e)
    {
        std::cout << "Exception has been thrown: " << e.what() << std::endl;
    }
    return -1;
}
