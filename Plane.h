/**
 * @file Plane.h
 * @author Tomas Polasek
 * @brief 
 */

#ifndef ECS_FIT_PLANE_H
#define ECS_FIT_PLANE_H

#include "Types.h"

/// Renderable plane.
class Plane
{
public:
    /// Construct the Triangle and all the buffers.
    Plane();

    /// Render the triangle
    void render();

    /// Destroy the buffers.
    ~Plane();
private:
    /// Destroy all the GL buffers.
    void destroy();

    /// 2 triangles -> 6 vertices.
    static constexpr unsigned int NUM_VERTICES{6};
    static constexpr unsigned int VALS_PER_VERTEX{3};

    /// Vertex data for the Plane.
    static const GLfloat VERTEX_BUFFER_DATA[NUM_VERTICES * VALS_PER_VERTEX];

    /// The vertex array ID.
    GLuint mVAId;
    /// The vertex buffer ID.
    GLuint mVBId;
protected:
}; // class Plane

#endif //ECS_FIT_PLANE_H
