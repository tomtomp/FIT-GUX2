/**
 * @file ColorSampler.h
 * @author Tomas Polasek
 * @brief Simple color sampler with binding to OpenGL.
 */

#include "ColorSampler.h"

ColorSampler::~ColorSampler()
{
    destroy();
}

void ColorSampler::fromCairoSurface(cairo_surface_t *surface)
{
    destroy();

    unsigned char *data = cairo_image_surface_get_data(surface);
    auto width = cairo_image_surface_get_width(surface);

    glGenTextures(1, &mTextureId);
    glBindTexture(GL_TEXTURE_1D, mTextureId);
    glTexImage1D(
            GL_TEXTURE_1D,
            0, GL_RGBA8,
            width, 0, GL_BGRA,
            GL_UNSIGNED_BYTE, data
    );
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void ColorSampler::use(GLint id)
{
    // Use texture unit 0.
    glUniform1i(id, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_1D, mTextureId);
}

void ColorSampler::destroy()
{
    if (mTextureId)
    {
        glDeleteTextures(1, &mTextureId);
    }
}
