/**
 * @file FramebufferImage.h
 * @author Tomas Polasek
 * @brief Class used for offscreen rendering.
 */

#ifndef FRACTALEXPLORER_FRAMEBUFFERIMAGE_H
#define FRACTALEXPLORER_FRAMEBUFFERIMAGE_H

#include "Types.h"

/// Class used for offscreen rendering into an image.
class FramebufferImage
{
public:
    FramebufferImage();
private:
    /// Framebuffer object identifier;
    GLuint mFbo;
    /// Renderbuffer object identifier;
    GLuint mRbo;
protected:
};


#endif //FRACTALEXPLORER_FRAMEBUFFERIMAGE_H
