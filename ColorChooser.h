/**
 * @file ColorChooser.cpp
 * @author Tomas Polasek
 * @brief Color chooser widget.
 */

#ifndef FRACTALEXPLORER_COLORCHOOSER_H
#define FRACTALEXPLORER_COLORCHOOSER_H

#include "Types.h"
#include "ColorSampler.h"

class ColorChooser
{
public:
    ColorChooser() = default;
    ~ColorChooser() = default;

    /**
     * Initialize the widget.
     * @param cs Color sampler used for containing the colors.
     * @param redraw Widget which should be redrawn each time
     *   the color sampler updates.
     */
    void init(ColorSampler *cs, GtkWidget *redraw);

    /// Update the sampler with current colors.
    void updateSampler();

    /// Get the main widget.
    GtkWidget *parent();
private:
    static const GdkRGBA START_COLOR;
    static const GdkRGBA START_FIRST_COLOR;
    static const GdkRGBA START_SECOND_COLOR;
    static constexpr unsigned int PATTERN_WIDTH{256};
    static constexpr unsigned int MIN_COLORS{2};

    /// Create elements of user interface.
    void createUI();

    /// Add new color to this chooser.
    void addColor(GdkRGBA color);

    /// Remove last color in the list.
    void removeColor();

    /**
     * Create a new cairo pattern using selected colors.
     * @param width Width of the created pattern.
     * @warning The returned value has to be freed, using cairo_pattern_destroy!
     * @return Returns the newly created pattern. Ownership is passed to the caller.
     */
    cairo_pattern_t *createCairoPattern(float width);

    /// Draw callback for color sampler.
    static bool samplerDraw(GtkWidget *app, cairo_t *cr, gpointer userData);
    /// Callback from selecting the first color.
    static void color1Set(GtkColorButton *btn, gpointer userData);
    /// Callback from selecting the second color.
    static void color2Set(GtkColorButton *btn, gpointer userData);
    /// Callback when any color changes.
    static void colorChanged(GtkColorButton *btn, gpointer userData);
    /// Callback from add color button.
    static void addColorBtn(GtkButton *btn, gpointer userData);
    /// Callback from remove color button.
    static void removeColorBtn(GtkButton *btn, gpointer userData);

    /// Color sampler controlled by this chooser.
    ColorSampler *mCS;
    /// Widget which should be redrawn each time the color sampler is updated.
    GtkWidget *mRedraw;
    /// Main widget.
    GtkWidget *mParent;
    /// Widget containing color selectors.
    GtkWidget *mColorList;
    /// Button for adding colors.
    GtkWidget *mAddButton;
    /// Button for removing colors.
    GtkWidget *mRemoveButton;
    /// List of active color buttons.
    std::vector<GtkWidget*> mColorButtons;
protected:
};


#endif //FRACTALEXPLORER_COLORCHOOSER_H
