/**
 * @file Types.h
 * @author Tomas Polasek
 * @brief Type defitions for this application.
 */

#ifndef FRACTALEXPLORER_TYPES_H
#define FRACTALEXPLORER_TYPES_H

// GTK includes
#include <gtk/gtk.h>

// OpenGL includes
#include <epoxy/gl.h>
#ifdef _WIN32
#   include <epoxy/wgl.h>
#else
#   include <epoxy/glx.h>
#endif

// Standard library includes
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <sstream>

/// Simple 2d point
class Point
{
public:
    constexpr Point(float xx, float yy) :
        mData{xx, yy}
    { }

    float &x()
    { return mData[0]; }
    const float &x() const
    { return mData[0]; }

    float &y()
    { return mData[1]; }
    const float &y() const
    { return mData[1]; }

    float *data()
    { return mData; }
    const float *data() const
    { return mData; }
private:
    float mData[2] = {0.0, 0.0};
protected:
};

#endif //FRACTALEXPLORER_TYPES_H
